---
Title: dploot
Homepage: https://github.com/zblurx/dploot
Repository: https://gitlab.com/kalilinux/packages/dploot
Architectures: all
Version: 2.7.1-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### python3-dploot
 
  Implement all the DPAPI logic of SharpDPAPI and DPAPI, usable
  with a Python interpreter.
 
 **Installed size:** `276 KB`  
 **How to install:** `sudo apt install python3-dploot`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-cryptography 
 * python3-impacket
 * python3-lxml
 * python3-pyasn1 
 {{< /spoiler >}}
 
 ##### dploot
 
 
 ```
 root@kali:~# dploot -h
 usage: dploot [-h] [-debug] [-quiet]
               {certificates,credentials,masterkeys,vaults,backupkey,rdg,sccm,triage,machinemasterkeys,machinecredentials,machinevaults,machinecertificates,machinetriage,browser,wifi,mobaxterm}
               ...
 
 DPAPI looting remotely in Python. Version 2.7.1
 
 positional arguments:
   {certificates,credentials,masterkeys,vaults,backupkey,rdg,sccm,triage,machinemasterkeys,machinecredentials,machinevaults,machinecertificates,machinetriage,browser,wifi,mobaxterm}
                         Action
     certificates        Dump users certificates from remote target
     credentials         Dump users Credential Manager blob from remote target
     masterkeys          Dump users masterkey from remote target
     vaults              Dump users Vaults blob from remote target
     backupkey           Backup Keys from domain controller
     rdg                 Dump users saved password information for
                         RDCMan.settings from remote target
     sccm                Dump SCCM secrets (NAA, Collection variables, tasks
                         sequences credentials) from remote target
     triage              Loot Masterkeys (if not set), credentials, rdg,
                         certificates, browser and vaults from remote target
     machinemasterkeys   Dump system masterkey from remote target
     machinecredentials  Dump system credentials from remote target
     machinevaults       Dump system vaults from remote target
     machinecertificates
                         Dump system certificates from remote target
     machinetriage       Loot SYSTEM Masterkeys (if not set), SYSTEM
                         credentials, SYSTEM certificates and SYSTEM vaults
                         from remote target
     browser             Dump users credentials and cookies saved in browser
                         from remote target
     wifi                Dump wifi profiles from remote target
     mobaxterm           Dump Passwords and Credentials from MobaXterm
 
 options:
   -h, --help            show this help message and exit
   -debug                Turn DEBUG output ON
   -quiet                Only output dumped credentials
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
