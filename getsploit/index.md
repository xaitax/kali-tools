---
Title: getsploit
Homepage: https://github.com/vulnersCom/getsploit
Repository: https://gitlab.com/kalilinux/packages/getsploit
Architectures: all
Version: 1.0.0-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### getsploit
 
  Inspired by searchsploit, getsploit combines two features: command line search
  and download.
   
  It allows you to search online for the exploits across all the most popular
  collections, including (but not limited to):
     - Exploit-DB,
     - Metasploit,
     - Packetstorm
   
  The most powerful feature of getsploit is the ability to immediately download
  the exploit source code in your working path.
 
 **Installed size:** `34 KB`  
 **How to install:** `sudo apt install getsploit`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-clint
 * python3-six
 * python3-texttable
 * python3-vulners
 {{< /spoiler >}}
 
 ##### getsploit
 
 
 ```
 root@kali:~# getsploit -h
 Usage: Exploit search and download utility
 
 Options:
   -h, --help            show this help message and exit
   -t, --title           Search JUST the exploit title (Default is description
                         and source code).
   -j, --json            Show result in JSON format.
   -m, --mirror          Mirror (aka copies) search result exploit files to the
                         subdirectory with your search query name.
   -c COUNT, --count=COUNT
                         Search limit. Default 10.
   -l, --local           Perform search in the local database instead of
                         searching online.
   -u, --update          Update getsploit.db database. Will be downloaded in
                         the script path.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
