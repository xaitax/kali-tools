---
Title: sploitscan
Homepage: https://github.com/xaitax/SploitScan
Repository: https://salsa.debian.org/pkg-security-team/SploitScan
Architectures: all
Version: 0.5.0+git20240429.5243d70-1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sploitscan
 
  SploitScan is a powerful and user-friendly tool designed to streamline the process of identifying exploits for known vulnerabilities and their respective exploitation probability. Empowering cybersecurity professionals with the capability to swiftly identify and apply known and test exploits. It's particularly valuable for professionals seeking to enhance their security measures or develop robust detection strategies against emerging threats.
   
  - **CVE Information Retrieval**: Fetches CVE details from the National Vulnerability Database.
  - **EPSS Integration**: Includes Exploit Prediction Scoring System (EPSS) data, offering a probability score for the likelihood of CVE exploitation, aiding in prioritization.
  - **Public Exploits Aggregation**: Gathers publicly available exploits, enhancing the understanding of vulnerabilities.
  - **CISA KEV**: Shows if the CVE has been listed in the Known Exploited Vulnerabilities (KEV) of CISA.
  - **Patching Priority System**: Evaluates and assigns a priority rating for patching based on various factors including public exploits availability.
  - **Multi-CVE Support and Export Options**: Supports multiple CVEs in a single run and allows exporting the results to HTML, JSON and CSV formats.
  - **Vulnerability Scanner Import**: Import vulnerability scans from popular vulnerability scanners and search directly for known exploits.
  - **AI-Powered Risk Assessment**: Leverages OpenAI to provide detailed risk assessments, potential attack scenarios, mitigation recommendations, and executive summaries.
  - **User-Friendly Interface**: Easy to use, providing clear and concise information.
  - **Comprehensive Security Tool**: Ideal for quick security assessments and staying informed about recent vulnerabilities.
 
 **Installed size:** `67 KB`  
 **How to install:** `sudo apt install sploitscan`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-tabulate
 {{< /spoiler >}}
 
 ##### sploitscan
 
 SploitScan is a tool designed to provide detailed information on vulnerabilities and associated exploits.
 
 ```
 root@kali:~# sploitscan -h
 
 ███████╗██████╗ ██╗      ██████╗ ██╗████████╗███████╗ ██████╗ █████╗ ███╗   ██╗
 ██╔════╝██╔══██╗██║     ██╔═══██╗██║╚══██╔══╝██╔════╝██╔════╝██╔══██╗████╗  ██║
 ███████╗██████╔╝██║     ██║   ██║██║   ██║   ███████╗██║     ███████║██╔██╗ ██║
 ╚════██║██╔═══╝ ██║     ██║   ██║██║   ██║   ╚════██║██║     ██╔══██║██║╚██╗██║
 ███████║██║     ███████╗╚██████╔╝██║   ██║   ███████║╚██████╗██║  ██║██║ ╚████║
 ╚══════╝╚═╝     ╚══════╝ ╚═════╝ ╚═╝   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═══╝
 v0.5 / Alexander Hagenah / @xaitax / ah@primepage.de
 
 usage: sploitscan [-h] [-e {json,csv}] cve_ids [cve_ids ...]
 
 SploitScan: Fetch and display data from NVD and public exploits for given CVE
 IDs.
 
 positional arguments:
   cve_ids               Enter one or more CVE IDs to fetch data. Separate
                         multiple CVE IDs with spaces. Format for each ID: CVE-
                         YYYY-NNNNN (Example: CVE-2023-23397 CVE-2024-12345)
 
 options:
   -h, --help            show this help message and exit
   -e {json,csv}, --export {json,csv}
                         Optional: Export the results to a JSON or CSV file.
                         Specify the format: 'json' or 'csv'.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
