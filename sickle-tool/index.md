---
Title: sickle-tool
Homepage: https://github.com/wetw0rk/Sickle
Repository: https://gitlab.com/kalilinux/packages/sickle-tool
Architectures: all
Version: 2.1-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### sickle-tool
 
  Sickle is a payload development tool originally created to aid in crafting
  shellcode, however it can be used in crafting payloads for other exploit
  types as well (non-binary). Although the current modules are mostly aimed
  towards assembly this tool is not limited to shellcode.
 
 **Installed size:** `86 KB`  
 **How to install:** `sudo apt install sickle-tool`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-capstone
 {{< /spoiler >}}
 
 ##### sickle-tool
 
 
 ```
 root@kali:~# sickle-tool -h
 usage: sickle-tool [-h] [-r READ] [-f FORMAT] [-s] [-e EXAMINE] [-obj OBJDUMP]
                    [-m MODULE] [-a ARCH] [-b BADCHARS] [-v VARNAME] [-l]
 
 Sickle - Payload development tool
 
 options:
   -h, --help            show this help message and exit
   -r READ, --read READ  read bytes from binary file (any file)
   -f FORMAT, --format FORMAT
                         output format (--list for more info)
   -s, --stdin           read ops from stdin (EX: echo -ne "\xde\xad\xbe\xef" |
                         sickle -s -f <format> -b '\x00')
   -e EXAMINE, --examine EXAMINE
                         examine a separate file containing original shellcode.
                         mainly used to see if shellcode was recreated
                         successfully
   -obj OBJDUMP, --objdump OBJDUMP
                         binary to use for shellcode extraction (via objdump
                         method)
   -m MODULE, --module MODULE
                         development module
   -a ARCH, --arch ARCH  select architecture for disassembly
   -b BADCHARS, --badchars BADCHARS
                         bad characters to avoid in shellcode
   -v VARNAME, --varname VARNAME
                         alternative variable name
   -l, --list            list all available formats and arguments
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
