---
Title: coercer
Homepage: https://github.com/p0dalirius/Coercer
Repository: https://gitlab.com/kalilinux/packages/coercer
Architectures: all
Version: 2.4.3-0kali1
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### coercer
 
  A python script to automatically coerce a Windows server to authenticate on an
  arbitrary machine through many methods.
 
 **Installed size:** `240 KB`  
 **How to install:** `sudo apt install coercer`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-impacket
 * python3-jinja2
 * python3-xlsxwriter
 {{< /spoiler >}}
 
 ##### coercer
 
 
 ```
 root@kali:~# coercer -h
        ______
       / ____/___  ___  _____________  _____
      / /   / __ \/ _ \/ ___/ ___/ _ \/ ___/
     / /___/ /_/ /  __/ /  / /__/  __/ /      v2.4.3
     \____/\____/\___/_/   \___/\___/_/       by @podalirius_
 
 usage: coercer [-h] [-v] {scan,coerce,fuzz} ...
 
 Automatic windows authentication coercer using various methods.
 
 positional arguments:
   {scan,coerce,fuzz}  Mode
     scan              Tests known methods with known working paths on all
                       methods, and report when an authentication is received.
     coerce            Trigger authentications through all known methods with
                       known working paths
     fuzz              Tests every method with a list of exploit paths, and
                       report when an authentication is received.
 
 options:
   -h, --help          show this help message and exit
   -v, --verbose       Verbose mode (default: False)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
