---
Title: vopono
Homepage: https://github.com/jamesmcm/vopono
Repository: https://gitlab.com/kalilinux/packages/vopono
Architectures: any
Version: 0.10.10-0kali4
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### vopono
 
  vopono is a tool to run applications through VPN tunnels via temporary
  network namespaces. This allows you to run only a handful of applications
  through different VPNs simultaneously, whilst keeping your main connection as
  normal.
 
 **Installed size:** `10.16 MB`  
 **How to install:** `sudo apt install vopono`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libgcc-s1 
 * nftables
 {{< /spoiler >}}
 
 ##### vopono
 
 
 ```
 root@kali:~# vopono -h
 Launch applications in a temporary VPN network namespace
 
 Usage: vopono [OPTIONS] <COMMAND>
 
 Commands:
   exec     Execute an application with the given VPN connection
   list     List running vopono namespaces and applications
   sync     Synchronise local server lists with VPN providers
   servers  List possible server configs for VPN provider, beginning with prefix
   help     Print this message or the help of the given subcommand(s)
 
 Options:
   -v, --verbose  Verbose output
   -A, --askpass  read sudo password from program specified in SUDO_ASKPASS environment variable
   -h, --help     Print help
   -V, --version  Print version
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
