---
Title: pyinstxtractor
Homepage: https://github.com/extremecoders-re/pyinstxtractor
Repository: https://gitlab.com/kalilinux/packages/pyinstxtractor
Architectures: all
Version: 2024.04-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### pyinstxtractor
 
  PyInstaller Extractor is a Python script to extract the contents of a
  PyInstaller generated executable file.
 
 **Installed size:** `28 KB`  
 **How to install:** `sudo apt install pyinstxtractor`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 {{< /spoiler >}}
 
 ##### pyinstxtractor
 
 
 ```
 root@kali:~# pyinstxtractor -h
 [!] Error: Could not open -h
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
