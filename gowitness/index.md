---
Title: gowitness
Homepage: https://github.com/sensepost/gowitness
Repository: https://gitlab.com/kalilinux/packages/gowitness
Architectures: amd64 arm64 armhf i386
Version: 2.5.1-0kali3
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### gowitness
 
  gowitness is a website screenshot utility written in Golang, that uses Chrome
  Headless to generate screenshots of web interfaces using the command line, with
  a handy report viewer to process results. Both Linux and macOS is supported,
  with Windows support mostly working.
   
  Inspiration for gowitness comes from Eyewitness. If you are looking for
  something with lots of extra features, be sure to check it out along with these
  other projects.
 
 **Installed size:** `27.05 MB`  
 **How to install:** `sudo apt install gowitness`  
 
 {{< spoiler "Dependencies:" >}}
 * chromium
 * libc6 
 {{< /spoiler >}}
 
 ##### gowitness
 
 
 ```
 root@kali:~# gowitness -h
 A commandline web screenshot and information gathering tool by @leonjza
 
 Usage:
   gowitness [command]
 
 Available Commands:
   completion  Generate the autocompletion script for the specified shell
   file        Screenshot URLs sourced from a file or stdin
   help        Help about any command
   merge       Merge gowitness sqlite databases
   nessus      Screenshot services from a Nessus XML file
   nmap        Screenshot services from an Nmap XML file
   report      Work with gowitness reports
   scan        Scan a CIDR range and take screenshots along the way
   server      Starts a webserver that serves the report interface, api and screenshot tool
   single      Take a screenshot of a single URL
   version     Prints the version of gowitness
 
 Flags:
       --chrome-path string       path to chrome executable to use
   -D, --db-location string       destination for the gowitness database. supports sqlite & postgres (eg: postgres://user:pass@host:port/db) (default "sqlite://gowitness.sqlite3")
       --debug                    enable debug logging
       --debug-db                 enable debug logging for all database operations
       --delay int                delay in seconds between navigation and screenshot
       --disable-db               disable all database operations
       --disable-logging          disable all logging
   -F, --fullpage                 take fullpage screenshots
       --header strings           additional HTTP header to set. Supports multiple --header flags
   -h, --help                     help for gowitness
       --js string                javascript code to execute when loading a target site (eg: console.log('gowitness'))
       --pdf                      save screenshots as pdf
   -p, --proxy string             http/socks5 proxy to use. Use format proto://address:port
   -X, --resolution-x int         screenshot resolution x (default 1440)
   -Y, --resolution-y int         screenshot resolution y (default 900)
       --screenshot-db-store      save screenshots to the database as well
       --screenshot-filter ints   http response codes to screenshot. this is a filter. by default all codes are screenshotted
   -P, --screenshot-path string   store path for screenshots (use . for pwd) (default "screenshots")
       --timeout int              preflight check timeout (default 10)
       --user-agent string        user agent string to use (default "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36")
 
 Use "gowitness [command] --help" for more information about a command.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
