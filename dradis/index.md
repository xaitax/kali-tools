---
Title: dradis
Homepage: https://dradis.com/ce/
Repository: https://gitlab.com/kalilinux/packages/dradis
Architectures: amd64
Version: 4.12.0-0kali1
Metapackages: kali-linux-everything kali-linux-large kali-tools-reporting 
Icon: images/dradis-logo.svg
PackagesInfo: |
 ### dradis
 
  Dradis is a tool to help you simplify reporting and collaboration.
   
    - Spend more time testing and less time reporting
    - Ensure consistent quality across your assessments
    - Combine the output of your favorite scanners
   
   Dradis is an open-source project released in 2007 that has been refined for
   over a decade by security professionals around the world.
 
 **Installed size:** `448.50 MB`  
 **How to install:** `sudo apt install dradis`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * bundler
 * git
 * libc6 
 * libgcc-s1 
 * libpq5 
 * libruby3.1t64 
 * libsqlite3-0 
 * libssl3t64 
 * libstdc++6 
 * lsof
 * pwgen
 * ruby 
 {{< /spoiler >}}
 
 ##### dradis
 
 
 ```
 root@kali:~# dradis -h
 [*] Please wait for the Dradis service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:3000
 
 ```
 
 - - -
 
 ##### dradis-stop
 
 
 ```
 root@kali:~# dradis-stop -h
 x dradis.service - Dradis web application
      Loaded: loaded (/usr/lib/systemd/system/dradis.service; disabled; preset: disabled)
      Active: failed (Result: exit-code) since Thu 2024-05-23 07:10:25 EDT; 3s ago
    Duration: 1.659s
     Process: 134289 ExecStartPre=/bin/sh -c test -e /var/lib/dradis/secretkey || (umask 077; echo "SECRET_KEY_BASE=$(pwgen -1 -s -n 100)" >/var/lib/dradis/secretkey) (code=exited, status=0/SUCCESS)
     Process: 134291 ExecStart=/usr/bin/bundle exec rails server (code=exited, status=1/FAILURE)
    Main PID: 134291 (code=exited, status=1/FAILURE)
         CPU: 1.626s
 
 May 23 07:10:25 kali bundle[134291]:         from /usr/lib/dradis/ruby/3.1.0/gems/railties-7.0.8.3/lib/rails/command/base.rb:87:in `perform'
 May 23 07:10:25 kali bundle[134291]:         from /usr/lib/dradis/ruby/3.1.0/gems/railties-7.0.8.3/lib/rails/command.rb:48:in `invoke'
 May 23 07:10:25 kali bundle[134291]:         from /usr/lib/dradis/ruby/3.1.0/gems/railties-7.0.8.3/lib/rails/commands.rb:18:in `<main>'
 May 23 07:10:25 kali bundle[134291]:         from <internal:/usr/lib/ruby/vendor_ruby/rubygems/core_ext/kernel_require.rb>:38:in `require'
 May 23 07:10:25 kali bundle[134291]:         from <internal:/usr/lib/ruby/vendor_ruby/rubygems/core_ext/kernel_require.rb>:38:in `require'
 May 23 07:10:25 kali bundle[134291]:         from /usr/lib/dradis/ruby/3.1.0/gems/bootsnap-1.18.3/lib/bootsnap/load_path_cache/core_ext/kernel_require.rb:30:in `require'
 May 23 07:10:25 kali bundle[134291]:         from bin/rails:4:in `<main>'
 May 23 07:10:25 kali systemd[1]: dradis.service: Main process exited, code=exited, status=1/FAILURE
 May 23 07:10:25 kali systemd[1]: dradis.service: Failed with result 'exit-code'.
 May 23 07:10:25 kali systemd[1]: dradis.service: Consumed 1.626s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}


```console
service dradis start
```

## Screenshots

![Dradis login screen](images/dradis-01.png)
![Dradis dashboard](images/dradis-02.png)
![Dradis issue list](images/dradis-03.png)
![Dradis methodologies](images/dradis-04.png)
